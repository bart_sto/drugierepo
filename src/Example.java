import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class Example {

    public static void main(String[] args) {
        File f = new File("plikOKtorymMowa");
        System.out.println("Czy plik istnieje: " + f.exists());
        Scanner scanner = new Scanner(System.in);
        String word = scanner.nextLine();
//        String name = scanner.nextLine();
//        Integer age = scanner.nextInt();
//        String sex = scanner.nextLine();


        try (PrintWriter writer = new PrintWriter(new FileOutputStream(f))) {

            while (!word.equals("quit")) {
                writer.println(word);
                word = scanner.nextLine();
            }


        } catch (FileNotFoundException fnfe) {

        }
    }
}
